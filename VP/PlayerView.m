//
//  PlayerView.m
//  VP
//
//  Created by Miso Takacs on 2/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "PlayerView.h"

@implementation PlayerView

@synthesize player;

+ (Class)layerClass
{
	return [AVPlayerLayer class];
}

- (void)startPlayer
{
    NSURL * url = [NSURL URLWithString:@"http://devimages.apple.com/iphone/samples/bipbop/bipbopall.m3u8"];
    self.player = [AVPlayer playerWithURL:url];
    [(AVPlayerLayer *)self.layer setPlayer:self.player];
}

- (void)play
{
    [[self player] play];
}

- (void)pause
{
    [[self player] pause];
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
