//
//  SmokViewController.m
//  VP
//
//  Created by Miso Takacs on 2/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SmokViewController.h"

@implementation SmokViewController

@synthesize mPlayerView;
@synthesize mPlayButton;
@synthesize mTapRec;

int playback = 0;

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
       
	// Do any additional setup after loading the view, typically from a nib.
    [mPlayerView startPlayer];
    [mTapRec addTarget:self action:@selector(tapped:)];
    [mPlayerView addGestureRecognizer:mTapRec];
    
}

- (void)tapped:(UITapGestureRecognizer*)recognizer {
    if (recognizer.state == UIGestureRecognizerStateEnded) {
        [self play:self];   
    }
}

- (IBAction)play:(id)sender
{
    if (playback == 1) {
        playback = 0;
        [mPlayerView pause];
    } else {
        playback = 1;
        [mPlayerView play];
    }
    
    [mPlayButton setTitle:(playback == 1 ? @"Pause" : @"Play")
                 forState:UIControlStateNormal]; 
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;
}

@end
