//
//  PlayerView.h
//  VP
//
//  Created by Miso Takacs on 2/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface PlayerView : UIView

@property (nonatomic, strong) AVPlayer * player;

+ (Class)layerClass;

- (void)startPlayer;
- (void)play;
- (void)pause;

@end
