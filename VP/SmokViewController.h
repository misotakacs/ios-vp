//
//  SmokViewController.h
//  VP
//
//  Created by Miso Takacs on 2/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

#import "PlayerView.h"

@interface SmokViewController : UIViewController

@property (nonatomic, retain) IBOutlet PlayerView *mPlayerView;
@property (nonatomic, retain) IBOutlet UIButton *mPlayButton;
@property (nonatomic, retain) IBOutlet UITapGestureRecognizer *mTapRec;

-(IBAction)play:(id)sender;

@end
